import os
import json
import socket
import sys
import math
import pickle
import time
from itertools import *

from qlearn import *


class NoobBot(object):

    def __init__(self, socket, name, key, s, qlearn):
        self.socket = socket
        self.name = name
        self.key = key
        self.s = s
        self.qlearn = qlearn

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        self.pieces = data['race']['track']['pieces']
        for i, piece in enumerate(self.pieces):
            piece['index'] = i
            piece['radius'] = piece.get('radius', 1.0)
            piece['angle'] = piece.get('angle', 0.0)
            if 'length' not in piece:
                piece['length'] = math.pi * piece['radius']  * (abs(piece['angle']) / 180.0)
        self.track_length = sum(piece['length'] for piece in self.pieces)
        self.absolute_position = 0
        self.crashed = False
        self.start_time = 0
        self.ping()

    def on_game_start(self, data):
        print("Race started", data)
        self.ping()

    def distance_to_next_curve(self, index, in_piece_position):
        current_piece = self.pieces[index]
        current_angle = current_piece['angle']
        current_radius = current_piece['radius']
        distance = current_piece['length'] - in_piece_position

#        if current_piece['radius'] == 0.0:
#            return 0, current_piece

        cycle_pieces = self.pieces[index+1:] + self.pieces[:index+1]
        for piece in cycle(cycle_pieces):
            if (piece['radius'] != current_radius or
                    piece['angle'] != current_angle):
                break
            distance += piece['length']
        return distance, piece
    
    def get_absolute_position(self, index, in_piece_position):
        return sum(piece['length'] for piece in self.pieces[:index]) + in_piece_position

    def on_car_positions(self, data):
        if self.crashed:
            self.ping()
            return

        now = time.time()
        my_car_position = (datum for datum in data if datum['id']['name'] == self.name).next()

        current_piece = self.pieces[my_car_position['piecePosition']['pieceIndex']]
        distance_to_next_curve, next_curve_piece = self.distance_to_next_curve(
                my_car_position['piecePosition']['pieceIndex'],
                my_car_position['piecePosition']['inPieceDistance'])
        absolute_position = self.get_absolute_position(
                my_car_position['piecePosition']['pieceIndex'],
                my_car_position['piecePosition']['inPieceDistance'])
        if abs(absolute_position - self.absolute_position) < 100:
            distance_traveled = abs(absolute_position - self.absolute_position)
        else:
            distance_traveled = self.track_length - self.absolute_position + absolute_position
        self.absolute_position = absolute_position
        
        state = {}
        state['radius'] = current_piece['radius'] * 0.03
        state['angle'] = current_piece['angle'] / 5.0
        state['distance_to_next_curve'] = distance_to_next_curve / 100.0
        state['next_radius'] = next_curve_piece['radius'] * 0.03
        state['next_angle'] = next_curve_piece['angle'] / 5.0
        state['slip_angle'] = my_car_position['angle'] / 5.0
        if self.start_time == 0:
            state['speed'] = 0
        else:
            state['speed'] = distance_traveled / (now - self.start_time) * 0.0167
        reward = state['speed']

        for key, val in state.iteritems():
            abs_val = abs(val)
            if abs_val <= 1:
                state[key] = 0
            else:
                log_val = int(round(math.log(abs_val)))
                if val < 0:
                    state[key] = -log_val
                else:
                    state[key] = log_val

        state = tuple(state.itervalues())

        if self.start_time == 0:
            self.qlearn.init(state)
            self.race_started = True
        else:
            print 'reward\t', reward,
            self.qlearn.run(state, reward)

        self.start_time = now

        self.throttle(self.qlearn.action * 0.05)
        print '\tthrottle\t', self.qlearn.action * 0.05
#        print self.qlearn.q_values

#        if distance_to_next_curve < 5:
#            self.throttle(0.5)
#        elif distance_to_next_curve < 50:
#            self.throttle(0.0)
#        elif distance_to_next_curve > 150:
#            self.throttle(1.0)
#        else:
#            self.throttle(0.85)

#        if abs(state['slip_angle']) > 0:
#            self.throttle(0.65)
#        else:
#            self.throttle(0.9)

    def on_crash(self, data):
        print("Someone crashed", data)
        print 'reward', -10000
        self.qlearn.run(self.qlearn.state, -10000)
        self.crashed = True
        self.ping()

    def on_spawn(self, data):
        print("Someone spawned", data)
        self.crashed = False
        self.ping()

    def on_game_end(self, data):
        print("Race ended", data)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'spawn': self.on_spawn
        }
        socket_file = self.s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


def main():
    if len(sys.argv) != 5:
#        print("Usage: ./run host port botname botkey")
        host, port, name, key = 'testserver.helloworldopen.com', '8091', 'caps', 'FygbHqIlou3OPw'
    else:
        host, port, name, key = sys.argv[1:5]

    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(host, port, name, key))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = NoobBot(s, name, key, s, qlearn)
    bot.run()


if __name__ == "__main__":
    if os.path.exists('q.pkl'):
        qlearn = QLearn(pickle.load(open('q.pkl', 'r')))
    else:
        qlearn = QLearn()
    for i in xrange(10):
        main()
        pickle.dump(qlearn.q_values, open('q.pkl', 'w'))
