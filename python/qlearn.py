from collections import defaultdict


class QLearn(object):
    def __init__(self, q_values=None):
        if q_values is None:
            self.q_values = defaultdict(float)
        else:
            self.q_values = q_values
        self.all_actions = range(21)
        self.action = 20

    def init(self, state):
        self.state = state
        self._set_action()
        self.action = max((self.q_values[(state, a)], a) for a in self.all_actions)[1]
        self.total_reward = 0
    
    def run(self, state, reward):
        if state == self.state:
            self.total_reward += reward
            return

        gamma = 0.8
        self.q_values[(self.state, self.action)] = self.total_reward + gamma * max(self.q_values[(state, a)] for a in self.all_actions)
        self.state = state
        self._set_action()
        self.total_reward = 0

    def _set_action(self):
        self.action = max((self.q_values[(self.state, a)], a) for a in self.all_actions)[1]
